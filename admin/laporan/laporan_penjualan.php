<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require '../../db/koneksi.php';
require '../fpdf182/fpdf.php';

$query = "SET @NO_URUT = 0";
mysqli_query($db, $query);

$query = "SELECT ( @NO_URUT := @NO_URUT + 1 ) id, no_pem, tgl_pem, usr_pem, FORMAT(tot_pem, 2) AS tot_pem FROM pembelian";
$result = mysqli_query($db, $query);
$data   = array();

while ($row = mysqli_fetch_assoc($result)) {
	array_push($data, $row);
}

$query = "SET @NO_URUT = NULL";
mysqli_query($db, $query);

$judul  = "Laporan Penjualan";
$header = [
	[
		'label'  => 'NO',
		'length' => 10,
		'align'  => 'C',
	],
	[
		'label'  => 'INVOICE',
		'length' => 50,
		'align'  => 'C',
	],
	[
		'label'  => 'TANGGAL',
		'length' => 40,
		'align'  => 'C',
	],
	[
		'label'  => 'MEMBER',
		'length' => 50,
		'align'  => 'C',
	],
	[
		'label'  => 'TOTAL',
		'length' => 40,
		'align'  => 'C',
	],
];

$align = [
	[
		'label'  => 'NO',
		'length' => 10,
		'align'  => 'C',
	],
	[
		'label'  => 'INVOICE',
		'length' => 50,
		'align'  => 'C',
	],
	[
		'label'  => 'TANGGAL',
		'length' => 40,
		'align'  => 'C',
	],
	[
		'label'  => 'MEMBER',
		'length' => 50,
		'align'  => 'L',
	],
	[
		'label'  => 'TOTAL',
		'length' => 40,
		'align'  => 'R',
	],
];

$pdf = new FPDF();

$pdf->AddPage();
$pdf->SetFont('Arial', 'B', '16');
$pdf->Cell(0, 10, $judul, '0', 1, 'C');

$pdf->SetFont('Arial', '', '10');
$pdf->SetFillColor(255, 0, 0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128, 0, 0);

foreach ($header as $kolom) {
	$pdf->Cell($kolom['length'], 7, $kolom['label'], 1, '0', $kolom['align'], true);
}
$pdf->Ln();

$pdf->SetFillColor(224, 235, 255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
$fill = false;

foreach ($data as $baris) {
	$i = 0;
	foreach ($baris as $cell) {
		$pdf->Cell($header[$i]['length'], 5, $cell, 1, '0', $align[$i]['align'], true);
		$i++;
	}
	$fill = !$fill;
	$pdf->Ln();
}

$dest = '';
$name = 'laporan-penjualan-' . date('YmdHis');

$pdf->Output($dest, $name, false);