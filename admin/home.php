<?php
	session_start();

	if (!isset($_SESSION['logedin'])) {
		header('Location: index.php');
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Aditya Ricki Julianto">
	<title>SIBW</title>
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/bootstrap/css/dashboard.css">
	<link rel="stylesheet" href="assets/fontawesome/fontawesome.min.css">
	<link rel="stylesheet" href="assets/webfonts/fa-solid-900.ttf">
	<link rel="stylesheet" href="assets/webfonts/fa-solid-900.woff">
	<link rel="stylesheet" href="assets/webfonts/fa-solid-900.woff2">
	<!-- font awesome -->
	<!-- <script src="https://kit.fontawesome.com/a076d05399.js"></script> -->
	<script src="assets/jquery/jquery.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-dark bar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  		<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="home.php">ADMIN</a>
	    <ul class="navbar-nav px-3">
		    <li class="nav-item text-nowrap">
		        <a class="nav-link" href="#">Sign Out</a>
		    </li>
	    </ul>
	</nav>

	<div class="container-fluid mt-4">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<?php include('./inc_content/sidebar_menu.php') ?>
				</div>
			</nav>

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<?php
					$link = filter_input(INPUT_GET, 'link', FILTER_SANITIZE_URL);

					if (empty($link)) {
						include './inc_content/main_home.php';
					} elseif ($link == 'kategori') {
						include './inc_content/main_kategori.php';
					} elseif ($link == 'produk') {
						include './inc_content/main_produk.php';
					} elseif ($link == 'member') {
						include './inc_content/main_member.php';
					} elseif ($link == 'validasi') {
						include './inc_content/main_validasi.php';
					} elseif ($link == 'laporan') {
						include './inc_content/main_laporan.php';
					}
				?>
			</main>
		</div>
	</div>

	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/feather/feather.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" type="text/javascript"></script> -->
	<script>
		feather.replace()
	</script>
</body>
</html>