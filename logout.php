<?php
	session_start();
	unset($_SESSION['email_usr']);
	unset($_SESSION['nm_usr']);
	session_unset();
	session_destroy();

	header('Location: index.php');