<div class="dflex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">Manajemen Member</h1>
</div>
<div id="message"></div>
<div class="col-lg-8">
	<button type="button" class="btn btn-success" style="margin-bottom: 5px;" id="btnTambah">
		<i class="fas fa-plus"></i> Add
	</button>

	<input type="search" class="form-control-sm py-3 px-4 border" value="" placeholder="Search..." id="textSearch" name="search">

	<div class="table-responsive">
		<table class="table table-hover">
			<thead class="thead-light">
				<tr>
					<th>No</th>
					<th>E-Mail</th>
					<th>Nama Member</th>
					<th style="text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody id="tbody"></tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modalMember" tabindex="-1" role="dialog" aria-labelledby="ModalMember" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #337AB7">
				<h5 class="modal-title" id="titleModalMember">
					Member
				</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="#" id="formMember" enctype="multipart/form-data">
				<div class="modal-body" style="background-color: #BBD6EC">
	      			<div id="divCrud">
	      				<input type="hidden" id="inputCrud" value="add">
	      			</div>
	      			<div id="divMemberId"></div>
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="email_usr" class="form-control-label">E-Mail</label>
								<input type="email" id="email_usr" name="email" placeholder="email@dot.com" maxlength="50" class="form-control" required>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="pas_usr" class="form-control-label">Password</label>
								<input type="password" id="pas_usr" name="password" placeholder="Password" maxlength="50" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="nm_usr" class="form-control-label">
									Nama Lengkap
								</label>
								<input type="text" name="fullname" id="nm_usr" placeholder="Nama lengkap" maxlength="50" class="form-control" required>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="tlp" class="form-control-label">
									No Telepon
								</label>
								<input type="text" name="tlp" id="tlp" placeholder="No Telepon" maxlength="50" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="almt_usr">Alamat</label>
								<textarea name="almt_usr" id="almt_usr" cols="30" rows="2" placeholder="Alamat" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
			            <div class="col-12">
			                <div class="form-group">
			                    <input type="file" id="memberImage" name="memberImage" placeholder="File" onchange="readURL(this)">
			                    <input type="text" id="imageOld" name="imageOld" class="d-none">
			                </div>
			            </div>
					</div>
					<div class="row">
			            <div class="col-6">
			                <div class = "form-group">
			                    <img id="previewMember" src="#" alt="" width="200px" onclick="$('#memberImage').trigger('click') return false" />
			                </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" type="submit"><i class="fas fa-save"></i> Save</button>
					<button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Delete -->
<!-- <div class="modal fade" id="modalDeleteMember" tabindex="-1" role="dialog" aria-labelledby="ModalDeleteMember" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #D9534F">
				<h5 class="modal-title" id="titleModalDeleteMember">
					Delete Member
				</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="background-color: #BBD6EC">
				<p style="color: red; font-size: larger; text-align: center;">Yakin menghapus data berikut?</p>
				<h3 id="dataNama" style="text-align: center; color: #D9534F;"></h3>
				<form action="#" id="formDeleteMember">
					<div class="row">
						<div class="col">
							<div class="form-group">
								<input type="hidden" name="formId" id="idDel">
								<input type="hidden" name="proc" id="proc02" value="">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button onclick="deleteMember()" class="btn btn-danger" type="button" data-dismiss="modal"><i class="fas fa-trash"></i> Delete</button>
				<button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times"></i> Cancel</button>
			</div>
		</div>
	</div>
</div> -->

<script>
	getMember = () => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_member',
            },
            dataType: 'json',
            success: (res) => {
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.email_usr}</td>
									<td>${val.nm_usr}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getMemberById(${val.id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteMember(${val.id})"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Belum ada member!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	insertMember = () => {
		let formData = new FormData()

		formData.append('form', 'add_member')
		formData.append('nm_usr', $('#nm_usr').val())
		formData.append('email_usr', $('#email_usr').val())
		formData.append('pas_usr', $('#pas_usr').val())
		formData.append('almt_usr', $('#almt_usr').val())
		formData.append('tlp', $('#tlp').val())
		formData.append('image', $('#memberImage')[0].files[0])

		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: formData,
            mimeTypes: 'multipart/form-data',
            contentType: false,
            processData: false,
	        dataType: 'json',
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getMember()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	reset()

	        	// tutup modal
	        	$('#modalMember').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err.message)
	        }
	    })
	}

    readURL = (input) => {
        if (input.files && input.files[0]) {
            var reader = new FileReader()
            reader.onload = function (e) {
                $('#previewMember').attr('src', e.target.result)
            }
            reader.readAsDataURL(input.files[0])
        }
    }

	getMemberById = (id) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'get_member_by_id',
            },
            dataType: 'json',
            success: (res) => {
            	$('#modalMember').modal('show')

		        $('#nm_usr').val(res.data.nm_usr)
		        $('#email_usr').val(res.data.email_usr)
		        $('#pas_usr').val(res.data.pas_usr)
		        $('#almt_usr').val(res.data.almt_usr)
		        $('#tlp').val(res.data.tlp)
		        $('#imageOld').val(res.data.photo)
		        $('#previewMember').attr('src', `http://localhost/kuliah/sibw-praktik/3183111056_adityaRickiJulianto_pertemuan-4/admin/${res.data.photo}`)

            	let elId = `<input type="hidden" class="form-control" id="inputMemberId" aria-describedby="id" value="${res.data.id}">`
            	let elCr = `<input type="hidden" id="inputCrud" value="edit">`

            	$('#divMemberId').empty()
            	$('#divMemberId').append(elId)
            	$('#divCrud').empty()
            	$('#divCrud').append(elCr)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	updateMember = () => {
		let formData = new FormData()

		formData.append('form', 'edit_member')
		formData.append('id', $('#inputMemberId').val())
		formData.append('nm_usr', $('#nm_usr').val())
		formData.append('email_usr', $('#email_usr').val())
		formData.append('pas_usr', $('#pas_usr').val())
		formData.append('almt_usr', $('#almt_usr').val())
		formData.append('tlp', $('#tlp').val())
		formData.append('image_old', $('#imageOld').val())
		formData.append('image', $('#memberImage')[0].files[0])

		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: formData,
            mimeTypes: "multipart/form-data",
            contentType: false,
            processData: false,
	        dataType: 'json',
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getMember()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// tutup modal
	        	$('#modalMember').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err)
	        }
	    })
	}

    reset = () => {
        $('#email_usr').val('')
        $('#pas_usr').val('')
        $('#nm_usr').val('')
        $('#almt_usr').val('')
        $('#tlp').val('')
        $('#memberImage').val('')
        $('#previewMember').attr('src', '')
    }

	// fungsi delete
	deleteMember = (id) => {
		$.ajax({
            type: 'POST',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'delete_member',
            },
            dataType: 'json',
            success: (res) => {
            	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getMember()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// fungsi search
	search = (text) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	text: text,
                form: 'search_member',
            },
            dataType: 'json',
            success: (res) => {
            	$('#tbody').empty()
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.email_usr}</td>
									<td>${val.nm_usr}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getMemberById(${val.id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteMember(${val.id})"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Member yang dicari tidak ada!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// ketika halaman siap,
	$(document).ready(() => {
		// panggil fungsi ini
		getMember()

		// ketika button tambah di klik
		$('#btnTambah').on('click', (e) => {
            let elCr = `<input type="hidden" id="inputCrud" value="add">`

            reset()

            $('#divMemberId').empty()
            $('#divCrud').empty()
            $('#divCrud').append(elCr)
	        $('#modalMember').modal('show')
		})

		// ambil event ketika form di submit
		$('#formMember').on('submit', (e) => {
			e.preventDefault()

			let crud = $('#inputCrud').val()

			if (crud == 'add') {
				// panggil fungsi insert
				insertMember()

				reset()
			} else if (crud == 'edit') {
				// panggil fungsi update
				updateMember()
			}
		})

		// search
		$('#textSearch').on('keyup', (e) => {
			search(e.target.value)
		})
	})
</script>