<?php

	include '../../db/koneksi.php';
	include './Kategori.php';
	include './Produk.php';
	include './Member.php';
	include './Validation.php';

	if (isset($_FILES['image'])) {
		$image = $_FILES['image'];
	} else {
		$image = null;
	}

	if (isset($_GET['form'])) {
	    $form = $_GET['form'];
	} elseif (isset($_POST['form'])) {
	    $form = $_POST['form'];
	}

	$kategori = new Kategori($db);
	if ($form == 'get_kategori') {
		$kategori->index();
	}
	elseif ($form == 'add_kategori') {
		$kategori->store($_POST);
	}
	elseif ($form == 'get_kategori_by_id') {
		$kategori->edit($_GET);
	}
	elseif ($form == 'edit_kategori') {
		$kategori->update($_POST);
	}
	elseif ($form == 'delete_kategori') {
		$kategori->delete($_POST);
	}
	elseif ($form == 'search_kategori') {
		$kategori->search($_GET);
	}

	$produk = new Produk($db);
	if ($form == 'get_produk') {
		$produk->index();
	}
	elseif ($form == 'add_produk') {
		$produk->store($_POST, $image);
	}
	elseif ($form == 'get_produk_by_id') {
		$produk->edit($_GET);
	}
	elseif ($form == 'edit_produk') {
		$produk->update($_POST, $image);
	}
	elseif ($form == 'delete_produk') {
		$produk->delete($_POST);
	}
	elseif ($form == 'search_produk') {
		$produk->search($_GET);
	}

	$member = new Member($db);
	if ($form == 'get_member') {
		$member->index();
	}
	elseif ($form == 'add_member') {
		$member->store($_POST, $image);
	}
	elseif ($form == 'get_member_by_id') {
		$member->edit($_GET);
	}
	elseif ($form == 'edit_member') {
		$member->update($_POST, $image);
	}
	elseif ($form == 'delete_member') {
		$member->delete($_POST);
	}
	elseif ($form == 'search_member') {
		$member->search($_GET);
	}

	$validation = new Validation($db);
	if ($form == 'get_validation') {
		$validation->index();
	}
	elseif ($form == 'search_validation') {
		$validation->search($_GET);
	}
	elseif ($form == 'status_up_validation') {
		$validation->statusUp($_POST);
	}