<?php
	include '../db/koneksi.php';
	include 'controller/Authentication.php';

	$user = filter_input(INPUT_POST, 'inEmail', FILTER_SANITIZE_URL);
	$pass = filter_input(INPUT_POST, 'inPassword', FILTER_SANITIZE_STRING);
	$auth = new Authentication($db);

	$auth->Login($user, $pass);