<div class="modal fade" id="laporan" tabindex="-1" aria-labelledby="laporanLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
  			<div class="modal-body">
				<form action="laporan/range.php" method="POST">
					<div class="h5">Range waktu</div>
					<input type="hidden" name="range">
			        <div class="row">
			            <div class="col-md-5">
			        		<div class="form-group">
				        		<input type="text" class="form-control" placeholder="YYYY-MM-DD" name="mulai" required>
				        		<div class="small text-primary">Tanggal mulai</div>
				        	</div>
				        </div>
			            <div class="col-md-5">
			        		<div class="form-group">
				        		<input type="text" class="form-control" placeholder="YYYY-MM-DD" name="selesai" required>
				        		<div class="small text-primary">Tanggal selesai</div>
				        	</div>
				        </div>
			            <div class="col-md-2">
			        		<button type="submit" class="btn btn-primary"><i class="fas fa-print"></i></button>
				        </div>
			        </div>
				</form>
		    </div>
      	</div>
    </div>
</div>
<div class="card">
	<div class="card-header h3">
		Cetak laporan
	</div>
	<div class="card-body">
		<a href="laporan/laporan_penjualan.php" class="btn btn-primary">Semua</a>
		<a href="#" class="btn btn-success" id="modalLaporan">Kondisi tertentu</a>
	</div>
</div>
<script>
	$(document).ready(() => {
		// ketika button tambah di klik
		$('#modalLaporan').on('click', (e) => {
	        $('#laporan').modal('show')
		})
	})
</script>