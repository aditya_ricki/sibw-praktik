-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 09, 2021 at 08:47 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sibw_praktik`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `nm_usr` varchar(35) NOT NULL,
  `log_usr` varchar(35) NOT NULL,
  `pas_usr` varchar(35) NOT NULL,
  `email_usr` varchar(35) NOT NULL,
  `almt_usr` varchar(35) NOT NULL,
  `kp_usr` char(6) NOT NULL,
  `kota_usr` varchar(35) NOT NULL,
  `tlp` varchar(20) NOT NULL,
  `rek` varchar(20) NOT NULL,
  `nmrek` varchar(35) NOT NULL,
  `bank` enum('Mandiri','BNI','CIMB','BCA','Bank Jabar','BRI','Danamon','Permata') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`id`, `nm_usr`, `log_usr`, `pas_usr`, `email_usr`, `almt_usr`, `kp_usr`, `kota_usr`, `tlp`, `rek`, `nmrek`, `bank`) VALUES
(11, 'Aditya Ricki Julianto', 'aditya', 'aditya', 'adityaric72@gmail.com', 'Watusigar', '55853', 'Yogyakarta', '088126689761', '123456789012', 'Aditya Ricki Julianto', 'BCA');

-- --------------------------------------------------------

--
-- Table structure for table `detail_checkout`
--

CREATE TABLE `detail_checkout` (
  `id_detail_checkout` int(11) NOT NULL,
  `checkout_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_checkout`
--

INSERT INTO `detail_checkout` (`id_detail_checkout`, `checkout_id`, `produk_id`, `qty`) VALUES
(10, 11, 10, 2),
(11, 11, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `nm_usr` varchar(255) NOT NULL,
  `log_usr` varchar(255) DEFAULT NULL,
  `pas_usr` varchar(255) NOT NULL,
  `email_usr` varchar(255) NOT NULL,
  `almt_usr` text,
  `kp_usr` varchar(255) DEFAULT NULL,
  `kota_usr` varchar(255) DEFAULT NULL,
  `tlp` varchar(255) DEFAULT NULL,
  `photo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `nm_usr`, `log_usr`, `pas_usr`, `email_usr`, `almt_usr`, `kp_usr`, `kota_usr`, `tlp`, `photo`) VALUES
(4, 'Aditya Ricki', NULL, 'aditya', 'adityaric72@gmail.com', 'Gunungkidul, Yogyakarta', NULL, NULL, '08812668976', 'member_img/20201226101059.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_pem` varchar(20) NOT NULL,
  `tgl_pem` date NOT NULL,
  `usr_pem` varchar(35) NOT NULL,
  `norek_pem` varchar(35) NOT NULL,
  `nmrek_pem` varchar(35) NOT NULL,
  `bankrek_pem` varchar(35) NOT NULL,
  `tot_pem` int(11) NOT NULL,
  `sts_pem` enum('VALIDATION','PACKING','SENDING','COMPELETE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `no_pem`, `tgl_pem`, `usr_pem`, `norek_pem`, `nmrek_pem`, `bankrek_pem`, `tot_pem`, `sts_pem`) VALUES
(1, '20210103071631', '2021-01-03', 'Aditya Ricki Julianto', '123456789012', 'Aditya Ricki Julianto', 'BCA', 410000, 'SENDING');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `br_id` int(11) NOT NULL,
  `br_nm` varchar(100) DEFAULT NULL,
  `br_item` int(11) DEFAULT NULL,
  `br_hrg` double DEFAULT NULL,
  `br_stok` int(11) DEFAULT NULL,
  `br_satuan` varchar(20) DEFAULT NULL,
  `br_sts` char(1) DEFAULT NULL,
  `ket` varchar(200) DEFAULT NULL,
  `br_kat` varchar(20) DEFAULT NULL,
  `br_gbr` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`br_id`, `br_nm`, `br_item`, `br_hrg`, `br_stok`, `br_satuan`, `br_sts`, `ket`, `br_kat`, `br_gbr`) VALUES
(10, 'Jersey Barcelona', 1, 170000, 500, 'Pcs', 'Y', 'Ori Thailand', 'PAK20201024100258', 'produk_img/20201226100646.jpeg'),
(11, 'Jersey Chelsea', 1, 70000, 5, 'Pcs', 'Y', 'Ori Vietnam', 'PAK20201024100258', 'produk_img/20201113152846.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `produk_kategori`
--

CREATE TABLE `produk_kategori` (
  `id` int(11) NOT NULL,
  `kategori_kode` varchar(20) DEFAULT NULL,
  `kategori_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_kategori`
--

INSERT INTO `produk_kategori` (`id`, `kategori_kode`, `kategori_nama`) VALUES
(9, 'PAK20201024100258', 'Pakaian'),
(10, 'TES20201025053619', 'Buah'),
(11, 'MAK20201113093417', 'Makanan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `email`, `password`) VALUES
(1, 'admin@sibw.com', 'admin123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_checkout`
--
ALTER TABLE `detail_checkout`
  ADD PRIMARY KEY (`id_detail_checkout`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`no_pem`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`br_id`);

--
-- Indexes for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kategori_kode` (`kategori_kode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `detail_checkout`
--
ALTER TABLE `detail_checkout`
  MODIFY `id_detail_checkout` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `br_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
