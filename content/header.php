<!--start: Container -->
<div class="container">
    <!--start: Row -->
    <div class="row">
        <!--start: Logo -->
        <div class="logo span3">
            <a class="brand" href="#"><img src="assets/images/brand/brand_logo30.png" alt="Logo"></a>
        </div>
        <!--end: Logo -->
        <!--start: Navigation -->
        <div class="span9">
            <div class="navbar navbar-inverse">
                <div class="navbar-inner">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="<?= empty($lnk) ? 'active' : '' ?>"><a href="index.php">Home</a></li>
                            <li class="<?= $lnk == 'produk' ? 'active' : '' ?>"><a href="index.php?link=produk">Produk Kami</a></li>
                            <li class="<?= $lnk == 'komentar' ? 'active' : '' ?>"><a href="index.php?link=komentar">Komentar Anda</a></li>
                            <li class="<?= $lnk == 'keranjang' ? 'active' : '' ?>"><a href="index.php?link=keranjang">Keranjang</a></li>
                            <?php if (empty($_SESSION['nm_usr']) && empty($_SESSION['email_usr'])) : ?>
                                <li class="<?= $lnk == 'login' ? 'active' : '' ?>"><a href="index.php?link=login">Login</a></li>
                            <?php else : ?>
                                <li><a href="logout.php">Logout</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end: Navigation -->
    </div>
    <!--end: Row -->
</div>
<!--end: Container-->
