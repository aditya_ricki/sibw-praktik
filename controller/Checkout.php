<?php
session_start();
require_once '../vendor/autoload.php';

/**
 *
 */
class Checkout
{
	protected $db;

	function __construct($db)
	{
		$this->db = $db;
	}

	public function checkout($data)
	{
        $nama         = $data['nm_usr'];
        $username     = $data['log_usr'];
        $password     = $data['pas_usr'];
        $email        = $data['email_usr'];
        $alamat       = $data['almt_usr'];
        $kodePos      = $data['kp_usr'];
        $kota         = $data['kota_usr'];
        $noTelpon     = $data['tlp'];
        $noRekening   = $data['rek'];
		$namaRekening = $data['nmrek'];
		$bank         = $data['bank'];
		$total        = $data['total'];

        // Set your Merchant Server Key
		Midtrans\Config::$serverKey = 'SB-Mid-server-liOLujIuAcy43syO-YRWQR86';
		// Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
		Midtrans\Config::$isProduction = false;
		// Set sanitization on (default)
		Midtrans\Config::$isSanitized = true;
		// Set 3DS transaction for credit card to true
		Midtrans\Config::$is3ds = true;

		$queryCheckout = "INSERT INTO checkout (nm_usr, log_usr, pas_usr, email_usr, almt_usr, kp_usr, kota_usr, tlp, rek, nmrek, bank) VALUES ('$nama', '$username', '$password', '$email', '$alamat', '$kodePos', '$kota', '$noTelpon', '$noRekening', '$namaRekening', '$bank')";
		$result      = $this->db->query($queryCheckout);
		$checkout_id = mysqli_insert_id($this->db);

        foreach ($_SESSION['items'] as $key => $item) {
            $queryDetailCheckout = "INSERT INTO detail_checkout (checkout_id, produk_id, qty) VALUES ('$checkout_id', '$key', '$item')";
            $this->db->query($queryDetailCheckout);
        }

		$no_pem          = date('Ymdhis');
		$tgl_pem         = date('Y-m-d');
		$sts_pem         = 'VALIDATION';
		$queryValidation = "INSERT INTO pembelian (no_pem, tgl_pem, usr_pem, norek_pem, nmrek_pem, bankrek_pem, tot_pem, sts_pem) VALUES ('$no_pem', '$tgl_pem', '$nama', '$noRekening', '$namaRekening', '$bank', '$total', '$sts_pem')";
		$result          = $this->db->query($queryValidation);

	    $params = [
	        'transaction_details' => [
	            'order_id'            => rand(),
	            'gross_amount'        => $total,
	        ],
	        'customer_details' => [
	            'first_name' => $nama,
	            'last_name'  => 'unknown',
	            'email'      => $email,
	            'phone'      => $noTelpon,
	        ],
	    ];

	    $snapToken = \Midtrans\Snap::getSnapToken($params);

        echo json_encode([
			'token' => $snapToken,
			'data'  => $data
        ]);
	}
}