<!-- start: Page Title -->
<div id="page-title">
    <div id="page-title-inner">
        <!-- start: Container -->
        <div class="container">
                <h2><i class="ico-camera ico-white"></i>Detail Produk</h2>
        </div>
        <!-- end: Container  -->
    </div>
</div>
<!-- end: Page Title -->
<!--start: Wrapper-->
<div id="wrapper">
    <!--start: Container -->
    <div class="container">
        <!-- start: Row -->
        <div class="row">
            <div class="col-sm-6">
                <?php
                    $kd = filter_input(INPUT_GET, 'kd', FILTER_SANITIZE_URL);
                    if(empty($kd)){
                        $kd = $_SESSION['kd'];
                    }
                ?>
                <input type="hidden" value="<?= $kd ?>" id="brId" />
                <!--<div class="span4">-->
                <!--<div class="icons-box">-->
                <div class="hero-unit" style="margin-left: 20px;">
                    <table>
                        <tr>
                            <td rowspan="6"><img id="brGbr" /></td>
                        </tr>
                        <tr>
                            <td colspan="6"><div class="title"><h3 id="brNm"></h3></div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td size="200"><h3>Harga</h3></td>
                            <td><h3>:</h3></td>
                            <td><div><h3 id="brHrg"></h3></div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><h3>Stock</h3></td>
                            <td><h3>:</h3></td>
                            <td>
                                <div>
                                    <h3 id="brStok">
                                    </h3>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><h3>Satuan</h3></td>
                            <td><h3>:</h3></td>
                            <td><div><h3 id="brSatuan"></h3></div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><h3>Keterangan</h3></td>
                            <td><h3>:</h3></td>
                            <td><div><h3 id="ket"></h3></div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><div class="clear"><a id="brBeli" class="btn btn-lg btn-danger">Beli &raquo;</a></div></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Row -->
    </div>
    <!--end: Container-->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" id="ongkir" method="POST">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Kota Asal: </label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="kota_asal" name="kota_asal" required="">
                                    </select>
                                </div>
                            </div>

                            <br>

                            <div class="form-group">
                                <label class="control-label col-sm-3">Kota Tujuan: </label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="kota_tujuan" name="kota_tujuan" required="">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <br>

                            <div class="form-group">
                                <label class="control-label col-sm-3">Kurir: </label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="kurir" name="kurir" required="">
                                        <option value="pos">POS INDONESIA</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                    </select>
                                </div>
                            </div>

                            <br>

                            <div class="form-group">
                                <label class="control-label col-sm-3">Berat (Kg): </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="berat" name="berat" required="">
                                    <button type="submit" class="btn btn-default">Cek Ongkir</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7" id="response_ongkir">
            </div>
        </div>
    </div>
</div>
<!-- end: Wrapper  -->
<script>
    getProductById = (id) => {
        $.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                id: id,
                form: 'get_produk_by_id',
            },
            dataType: "json",
            success: (res) => {
                $('#brNm').html(res.data.br_nm)
                $('#brHrg').html(res.data.br_hrg)
                $('#brStok').html(res.data.br_stok)
                $('#brSatuan').html(res.data.br_satuan)
                $('#ket').html(res.data.ket)
                $('#brGbr').attr('src', `http://localhost/kuliah/sibw-praktik/3183111056_adityaRickiJulianto_pertemuan-4/admin/${res.data.br_gbr}`)
                $('#brBeli').attr('href', `cart.php?act=add&amp;barang_id=${res.data.br_id}&amp;ref=index.php?link=keranjang&kd=${res.data.br_id}`)
            },
            error: (err) => {
                console.log(err)
            }
        })
    }

    $(document).ready(() => {
        const id = $('#brId').val()

        getProductById(id)

        $('#kota_asal').select2({
            placeholder: 'Pilih kota/kabupaten asal', language: "id"
        })

        $('#kota_tujuan').select2({
            placeholder: 'Pilih kota/kabupaten tujuan', language: "id"
        })

        $.ajax({
            type: "GET",
            dataType: "html",
            url: "controller/RajaOngkir.php?q=kotaasal",
            success: function(msg){
                $("select#kota_asal").html(msg)
            }
        })

        $.ajax({
            type: "GET",
            dataType: "html",
            url: "controller/RajaOngkir.php?q=kotatujuan",
            success: function(msg){
                $("select#kota_tujuan").html(msg)
            }
        })

        $("#ongkir").submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: 'controller/RajaOngkir.php',
                type: 'post',
                data: $(this).serialize(),
                success: function(data) {
                console.log(data)
                    document.getElementById("response_ongkir").innerHTML = data
                }
            })
        })
    })
</script>