<!-- Modal Add -->
<div class="modal fade" id="addData" tabindex="-1" aria-labelledby="addDataLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="exampleModalLabel">Tambah data</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
	        <form action="#" id="formData">
	      		<div class="modal-body">
	      			<div id="divCrud">
	      				<input type="hidden" id="inputCrud" value="add">
	      			</div>
	      			<div id="divKategoriId"></div>
				  	<div class="form-group">
					    <label for="inputKategoriNama">Nama kategori</label>
					    <input type="text" class="form-control" id="inputKategoriNama" aria-describedby="kategori_nama">
				  	</div>
	      		</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary">Submit</button>
	      		</div>
			</form>
    	</div>
  	</div>
</div>
<div class="card">
	<div class="card-header">
		Data Kategori
		<div class="float-right">
			<div class="input-group mb-3">
			  	<div class="input-group-prepend">
			      	<button class="btn btn-success btn-sm d-inline" data-toggle="modal" data-target="#addData" id="btnTambah"><i class="fas fa-plus"></i></button>
			  	</div>
			  	<input type="text" class="form-control" aria-label="Text input with checkbox" id="textSearch" style="outline: none;" placeholder="Search">
			</div>
		</div>
	</div>
	<div class="card-body">
		<div id="message"></div>
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<th style="border-top: none;">No</th>
					<th style="border-top: none;">Kode</th>
					<th style="border-top: none;">Nama</th>
					<th style="border-top: none;">Aksi</th>
				</thead>
				<tbody id="tbody"></tbody>
			</table>
		</div>
	</div>
</div>
<script>
	// function untuk mengambil semua data kategori
	getKategori = () => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
                form: 'get_kategori',
            },
            dataType: "json",
            success: (res) => {
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.kategori_kode}</td>
									<td>${val.kategori_nama}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getKategoriById(${val.id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteKategori(${val.id})"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Belum ada kategori!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// function untuk menambah data
	insertKategori = () => {
		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: {
	            form: 'add_kategori',
	            kategori_nama: $('#inputKategoriNama').val()
	        },
	        dataType: "json",
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getKategori()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// tutup modal
	        	$('#addData').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err)
	        }
	    })
	}

	// function untuk mengambil data kategori berdasarkan id
	getKategoriById = (id) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'get_kategori_by_id',
            },
            dataType: "json",
            success: (res) => {
            	$('#addData').modal('show')
            	$('#inputKategoriNama').val(res.data.kategori_nama)

            	let elId = `<input type="hidden" class="form-control" id="inputKategoriId" aria-describedby="id" value="${res.data.id}">`
            	let elCr = `<input type="hidden" id="inputCrud" value="edit">`

            	$('#divKategoriId').empty()
            	$('#divKategoriId').append(elId)
            	$('#divCrud').empty()
            	$('#divCrud').append(elCr)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// fungsi untuk update kategori
	updateKategori = () => {
		$.ajax({
	        type: 'POST',
	        url: 'controller/Ajax.php',
	        data: {
	            form: 'edit_kategori',
	            kategori_nama: $('#inputKategoriNama').val(),
	            id: $('#inputKategoriId').val()
	        },
	        dataType: "json",
	        success: (res) => {
	        	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getKategori()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// tutup modal
	        	$('#addData').modal('hide')

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
	        },
	        error: (err) => {
	        	console.log(err)
	        }
	    })
	}

	// fungsi delete
	deleteKategori = (id) => {
		$.ajax({
            type: 'POST',
            url: 'controller/Ajax.php',
            data: {
            	id: id,
                form: 'delete_kategori',
            },
            dataType: "json",
            success: (res) => {
            	// buat elemen
	        	let el = `<div class="alert alert-success alert-dismissible fade show" role="alert">
							  	<strong>${res.message}</strong>
							  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    	<span aria-hidden="true">&times;</span>
							  	</button>
							</div>`

				// kosongkan isi elemen tbody
				$('#tbody').empty()

				// panggil fungsi untuk get data
				getKategori()

				// tampilkan pesan / alert
	        	$('#message').append(el)

	        	// hapus alert
	        	setTimeout(() => {
	        		$('#message').empty()
	        	}, 3000)
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// fungsi search
	search = (text) => {
		$.ajax({
            type: 'GET',
            url: 'controller/Ajax.php',
            data: {
            	text: text,
                form: 'search_kategori',
            },
            dataType: "json",
            success: (res) => {
            	$('#tbody').empty()
            	if (res.data.length != 0) {
	            	res.data.forEach((val, i) => {
	            		let el = `<tr>
									<td>${i + 1}</td>
									<td>${val.kategori_kode}</td>
									<td>${val.kategori_nama}</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-warning btn-sm" onclick="getKategoriById(${val.id})"><i class="fas fa-edit text-white"></i></button>
											<button class="btn btn-danger btn-sm" onclick="deleteKategori(${val.id})"><i class="fas fa-trash"></i></button>
										</div>
									</td>
								</tr>`

						$('#tbody').append(el)
	            	})
	            } else {
	            	let el = `<tr>
								<td colspan="4" class="text-center">Kategori yang dicari tidak ada!</td>
							</tr>`

					$('#tbody').append(el)
	            }
            },
            error: (err) => {
            	console.log(err)
            }
        })
	}

	// ketika halaman siap,
	$(document).ready(() => {
		// panggil fungsi ini
		getKategori()

		// ketika button tambah di klik
		$('#btnTambah').on('click', (e) => {
            let elCr = `<input type="hidden" id="inputCrud" value="add">`

            $('#inputKategoriNama').val('')
            $('#divKategoriId').empty()
            $('#divCrud').empty()
            $('#divCrud').append(elCr)
		})

		// ambil event ketika form di submit
		$('#formData').on('submit', (e) => {
			e.preventDefault()

			let crud = $('#inputCrud').val()

			if (crud == 'add') {
				// panggil fungsi insert
				insertKategori()
			} else if (crud == 'edit') {
				// panggil fungsi update
				updateKategori()
			}
		})

		// search
		$('#textSearch').on('keyup', (e) => {
			search(e.target.value)
		})
	})
</script>
