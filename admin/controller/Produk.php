<?php

/**
 *
 */
class Produk
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function index()
	{
        $query = "SELECT * FROM produk INNER JOIN produk_kategori ON produk.br_kat = produk_kategori.kategori_kode";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
        	'success' => true,
        	'data' => $data
        ]);
	}

    private function upload($image)
    {
        $ekstensi_diperbolehkan = ['png','jpg', 'jpeg'];
        $nama                   = $image['name'];
        $x                      = explode('.', $nama);
        $ekstensi               = strtolower(end($x));
        $ukuran                 = $image['size'];
        $file_tmp               = $image['tmp_name'];
        $newName                = 'produk_img/' . date('YmdHis') . '.' . $ekstensi;
        if (in_array($ekstensi, $ekstensi_diperbolehkan)) {
                move_uploaded_file($file_tmp, '../' . $newName);

                return $newName;
        } else {
            return false;
        }
    }

	public function store($data, $image)
	{
        $br_nm     = $data['br_nm'];
        $br_hrg    = $data['br_hrg'];
        $br_stok   = $data['br_stok'];
        $ket       = $data['ket'];
        $br_kat    = $data['br_kat'];
        $br_item   = 1;
        $br_satuan = 'Pcs';
        $br_sts    = 'Y';

        $newName   = $this->upload($image);

        if (!$newName) {
            die();
        }

		try {
            $query = "INSERT INTO produk (br_nm, br_hrg, br_stok, ket, br_kat, br_gbr, br_item, br_satuan, br_sts) VALUES ('$br_nm', '$br_hrg', '$br_stok', '$ket', '$br_kat', '$newName', $br_item, '$br_satuan', '$br_sts')";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Produk created!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function edit($data)
	{
		$id     = $data['id'];
		$query  = "SELECT * FROM produk WHERE br_id = $id";
		$result = $this->db->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo json_encode([
				'success' => true,
				'data' => $row
			]);
        }
	}

	public function update($data, $image)
	{
        $br_id     = $data['id'];
        $br_nm     = $data['br_nm'];
        $br_hrg    = $data['br_hrg'];
        $br_stok   = $data['br_stok'];
        $ket       = $data['ket'];
        $br_kat    = $data['br_kat'];
        $br_item   = 1;
        $br_satuan = 'Pcs';
        $br_sts    = 'Y';
        $image_old = $data['image_old'];

		try {
            if ($image != null) {
                $br_gbr = $this->upload($image);

                if (!$br_gbr) {
                    die();
                }

                unlink('../' . $image_old);
            } else {
                $br_gbr = $image_old;
            }
            $query = "UPDATE produk SET br_nm='$br_nm', br_hrg='$br_hrg', br_stok='$br_stok', ket='$ket', br_kat='$br_kat', br_item='$br_item', br_satuan='$br_satuan', br_sts='$br_sts', br_gbr='$br_gbr' WHERE br_id='$br_id'";
            $result = $this->db->query($query);

            echo json_encode([
            	'success' => true,
            	'message' => 'Product updated!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

	public function delete($data)
	{
        $id     = $data['id'];
        $br_gbr = $data['br_gbr'];

		try {
            $query = "DELETE FROM produk WHERE br_id='$id'";
            $result = $this->db->query($query);

            unlink('../' . $br_gbr);

            echo json_encode([
            	'success' => true,
            	'message' => 'Product deleted!'
            ]);
        } catch (\Throwable $th) {
            print_r($th);
            die;
        }
        die();
	}

    public function search($data)
    {
        $text = $data['text'];

        $query = "SELECT * FROM produk INNER JOIN produk_kategori ON produk.br_kat = produk_kategori.kategori_kode WHERE br_nm LIKE '%$text%' OR br_hrg LIKE '%$text%' OR kategori_nama LIKE '%$text%'";
        $result = $this->db->query($query);

        $data = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        echo json_encode([
            'success' => true,
            'data' => $data
        ]);
    }
}