<div id="da-slider" class="da-slider">
    <div class="da-slide">
        <h2>Batik Keluarga</h2>
        <p>Batik casual untuk keluarga dengan perkermbangan mode terbaru, nyaman dengan bahan dan jahitan kualitas terjamin...</p>
        <a href="#" class="da-link">Lihat Produk</a>
        <div class="da-img"><img src="assets/images/sliders/sarimbit01.png" alt="image01" /></div>
    </div>
    <div class="da-slide">
        <h2>Batik Casual</h2>
        <p>Kami memiliki banyak koleksi Batik untuk pasangan muda yang elegant dan modern, cocok dipakai sehari hari, berkunjung ke keluarga, dan pesta....</p>
        <a href="#" class="da-link">Lihat Produk</a>
        <div class="da-img"><img src="assets/images/sliders/sarimbit02.png" alt="image02" /></div>
    </div>
    <div class="da-slide">
        <h2>Batik Energik</h2>
        <p>Kami memiliki koleksi Batik untuk pasangan muda yang praktis, di desain dengan menyesuaikan kondisi dan cuaca yang beragam....silahkan coba..gratis.</p>
        <a href="#" class="da-link">Lihat Produk</a>
        <div class="da-img"><img src="assets/images/sliders/sarimbit03.png" alt="image03" /></div>
    </div>
    <div class="da-slide">
        <h2>Jreng Batik</h2>
        <p>Kami menerima pembuatan kaos custom sesuai dengan design keinginan anda dengan harga yang bisa di sesuaikan dengan kebutuhan anda.</p>
        <a href="#" class="da-link">Lihat Produk</a>
        <div class="da-img"><img src="assets/images/sliders/sarimbit04.png" alt="image04" /></div>
    </div>
    <nav class="da-arrows">
        <span class="da-arrows-prev"></span>
        <span class="da-arrows-next"></span>
    </nav>
</div>