<!-- start: Container -->
<div class="container">
    <!-- start: Row -->
    <div class="row">
        <!-- start: About -->
        <div class="span3">
            <h3>Tentang SIBW</h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis suscipit ex, ultrices suscipit purus tincidunt eu. Morbi in quam a libero rhoncus facilisis in sed risus.
            </p>
        </div>
        <!-- end: About -->
        <div class="span3">
        </div>
        <div class="span6">
            <!-- start: Follow Us -->
            <h3>Follow Us!</h3>
            <ul class="social-grid">
                <li>
                    <div class="social-item">
                        <div class="social-info-wrap">
                            <div class="social-info">
                                <div class="social-info-front social-twitter">
                                    <a href="http://twitter.com"></a>
                                </div>
                                <div class="social-info-back social-twitter-hover">
                                    <a href="http://twitter.com"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-item">
                        <div class="social-info-wrap">
                            <div class="social-info">
                                <div class="social-info-front social-facebook">
                                    <a href="http://facebook.com"></a>
                                </div>
                                <div class="social-info-back social-facebook-hover">
                                    <a href="http://facebook.com"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-item">
                        <div class="social-info-wrap">
                            <div class="social-info">
                                <div class="social-info-front social-dribbble">
                                    <a href="http://dribbble.com"></a>
                                </div>
                                <div class="social-info-back social-dribbble-hover">
                                    <a href="http://dribbble.com"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="social-item">
                        <div class="social-info-wrap">
                            <div class="social-info">
                                <div class="social-info-front social-flickr">
                                    <a href="http://flickr.com"></a>
                                </div>
                                <div class="social-info-back social-flickr-hover">
                                    <a href="http://flickr.com"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- end: Follow Us -->
        </div>
    </div>
    <!-- end: Row -->
</div>
<!-- end: Container  -->
<!-- start: Container -->
<div class="container">
    <!-- start: Row -->
    <div class="row">
        <!-- start: Footer Menu Logo -->
        <div class="span2">
            <div id="footer-menu-logo">
                <a href="#"><img src="assets/images/brand/brand_logo30.png" alt="logo" /></a>
            </div>
        </div>
        <!-- end: Footer Menu Logo -->
        <!-- start: Footer Menu Links-->
        <div class="span9">
            <div id="footer-menu-links">
                <ul id="footer-nav">
                    <li><a href="#">Batik Terlaris</a></li>
                    <li><a href="#">Batik Terbaru</a></li>
                    <li><a href="#">Batik Sarimbit</a></li>
                    <li><a href="#">Batik Keluarga</a></li>
                    <li><a href="#">Batik Muda</a></li>
                </ul>
            </div>
        </div>
        <!-- end: Footer Menu Links-->
        <!-- start: Footer Menu Back To Top -->
        <div class="span1">
            <div id="footer-menu-back-to-top">
                <a href="#"></a>
            </div>
        </div>
        <!-- end: Footer Menu Back To Top -->
    </div>
    <!-- end: Row -->
</div>
<!-- end: Container  -->